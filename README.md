# cave

Docker image providing [bashcov](https://github.com/infertux/bashcov) & [bats](https://github.com/bats-core/bats-core) as one command.

| Bats Library                                              | Image Location    |
|:---------------------------------------------------------:|:-----------------:|
| [bats-core](https://github.com/bats-core/bats-core)       | /opt/bats         |
| [bats-support](https://github.com/bats-core/bats-support) | /opt/bats-support |
| [bats-assert](https://github.com/bats-core/bats-assert)   | /opt/bats-assert  |
| [bats-detik](https://github.com/bats-core/bats-detik)     | /opt/bats-detik   |
| [bats-file](https://github.com/bats-core/bats-file)       | /opt/bats-file    |

## Usage

```bash
docker run -v "/project/path":/mnt/workspace --rm --name cave \
    registry.gitlab.com/agogpixel/devops/docker/cave:latest \
    [--skip-uncovered] [--command-name <name>] \
    [-cr] [-f <regex>] [-j <jobs>] <test>...
```

`<test>` is the path to a `bats` test file, or the path to a directory containing `bats` test files (ending with `.bats`). Coverage report generated at `./coverage`.

### Options

Subset of `bashcov` & `bats` options are supported.

#### bashcov

| Option                  | Description                                  |
|:-----------------------:|:--------------------------------------------:|
| `--skip-uncovered`      | Skip inclusion of uncovered files in report. |
| `--command-name <name>` | Specify command name for coverage report.    |

#### bats

| Option                            | Description                                                 |
|:---------------------------------:|:-----------------------------------------------------------:|
| `-c` , `--count`                  | Count the number of test cases without running any tests.   |
| `-f <regex>` , `--filter <regex>` | Filter test cases by names matching the regular expression. |
| `-j <jobs>` , `--jobs <jobs>`     | Number of parallel jobs to run.                             |
| `-r` , `--recursive`              | Include tests in subdirectories.                            |

## Development

- Project utilizes [GitHub flow](https://guides.github.com/introduction/flow/).
- Images built and released to project's [container registry](https://gitlab.com/agogpixel/devops/docker/cave/container_registry).
