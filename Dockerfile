################################################################################
# Global Arguments
################################################################################
ARG ALPINE_TAG=3.11
ARG BATS_VERSION=1.2.0
ARG BATS_SUPPORT_VERSION=0.3.0
ARG BATS_ASSERT_VERSION=2.0.0
ARG BATS_DETIK_VERSION=1.0.0
ARG BATS_FILE_VERSION=0.3.0

################################################################################
# Builder Image
################################################################################
FROM registry.gitlab.com/agogpixel/devops/docker/alpine-builder:${ALPINE_TAG} as builder

# Build bats.
ARG BATS_VERSION
RUN curl -L https://github.com/bats-core/bats-core/archive/v${BATS_VERSION}.tar.gz | tar xz && \
  mv bats-core-${BATS_VERSION} bats

# Build bats-support.
ARG BATS_SUPPORT_VERSION
RUN curl -L https://github.com/bats-core/bats-support/archive/v${BATS_SUPPORT_VERSION}.tar.gz | tar xz && \
  mv bats-support-${BATS_SUPPORT_VERSION} bats-support

# Build bats-assert.
ARG BATS_ASSERT_VERSION
RUN curl -L https://github.com/bats-core/bats-assert/archive/v${BATS_ASSERT_VERSION}.tar.gz | tar xz && \
  mv bats-assert-${BATS_ASSERT_VERSION} bats-assert

# Build bats-detik.
ARG BATS_DETIK_VERSION
RUN curl -L https://github.com/bats-core/bats-detik/archive/v${BATS_DETIK_VERSION}.tar.gz | tar xz && \
  mv bats-detik-${BATS_DETIK_VERSION} bats-detik

# Build bats-file.
ARG BATS_FILE_VERSION
RUN curl -L https://github.com/bats-core/bats-file/archive/v${BATS_FILE_VERSION}.tar.gz | tar xz && \
  mv bats-file-${BATS_FILE_VERSION} bats-file

# Build app.
COPY . /usr/local/src/cave/

################################################################################
# App Image
################################################################################
FROM ruby:2.7.1-alpine${ALPINE_TAG}

# Copy artifacts from builder.
COPY --from=builder /usr/local/src/bats /opt/bats
COPY --from=builder /usr/local/src/bats-support /opt/bats-support
COPY --from=builder /usr/local/src/bats-assert /opt/bats-assert
COPY --from=builder /usr/local/src/bats-detik /opt/bats-detik
COPY --from=builder /usr/local/src/bats-file /opt/bats-file
COPY --from=builder /usr/local/src/cave/cave.sh /usr/local/bin

ARG BATS_VERSION
ENV BATS_VERSION=${BATS_VERSION}

ARG BATS_SUPPORT_VERSION
ENV BATS_SUPPORT_VERSION=${BATS_SUPPORT_VERSION}

ARG BATS_ASSERT_VERSION
ENV BATS_ASSERT_VERSION=${BATS_ASSERT_VERSION}

ARG BATS_DETIK_VERSION
ENV BATS_DETIK_VERSION=${BATS_DETIK_VERSION}

ARG BATS_FILE_VERSION
ENV BATS_FILE_VERSION=${BATS_FILE_VERSION}

# Setup app environment.
RUN echo http://dl-cdn.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
  apk update && \
  apk add --no-cache \
    bash \
    curl \
    docker \
    git \
    parallel \
  && \
  rm -fR /var/cache/apk/* && \
  # Setup bashcov.
  gem install bashcov && \
  # Setup bats bin path.
  ln -s /opt/bats/bin/bats /usr/local/bin/bats && \
  # Ensure bats can find its libs (BATS_ROOT resolves here due to WORKDIR?).
  ln -s /opt/bats/libexec /mnt/libexec && \
  # Prepare binary.
  chmod +x /usr/local/bin/cave.sh && \
  ln -s /usr/local/bin/cave.sh /usr/local/bin/cave

# Runs:
#   bashcov [options] -- bats [options] <test>...
# in /mnt/workspace as guest user.
WORKDIR /mnt/workspace
USER guest
ENTRYPOINT ["cave"]
