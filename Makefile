################################################################################
# Settings
################################################################################

VENDOR_NAME=agogpixel
APP_NAME=cave
CR_HOST=registry.gitlab.com
GROUP_PATH=devops/docker

################################################################################
# Parameters
################################################################################

IMAGE_TAG?=latest
TEST_PATH?=test/cave.bats

CR_USER?=guest
CR_PASSWORD?=itsasecrettoeveryone

################################################################################
# Computed Values
################################################################################

GIT_SHORT_COMMIT_HASH=$(shell git diff-index --quiet HEAD && git rev-parse --short HEAD)

IMAGE_NAME=$(VENDOR_NAME)/$(APP_NAME)
CR_IMAGE_NAME=$(CR_HOST)/$(VENDOR_NAME)/$(GROUP_PATH)/$(APP_NAME)

IMAGE=$(IMAGE_NAME):$(IMAGE_TAG)
CR_IMAGE=$(CR_IMAGE_NAME):$(IMAGE_TAG)

################################################################################
# Rules
################################################################################

build: Dockerfile cave.sh
	docker build -t $(IMAGE) .
ifneq ($(GIT_SHORT_COMMIT_HASH),)
	docker tag $(IMAGE) $(IMAGE_NAME):$(GIT_SHORT_COMMIT_HASH)
endif
ifneq ($(IMAGE_TAG)),latest)
	docker tag $(IMAGE) $(IMAGE_NAME):latest
endif

test: build
	rm -rf "$(CURDIR)/coverage"
	docker run -v "$(CURDIR)":/mnt/workspace --rm --name $(APP_NAME) $(IMAGE) --skip-uncovered $(TEST_PATH)

release: login test tag
	docker push $(CR_IMAGE)
ifneq ($(GIT_SHORT_COMMIT_HASH),)
	docker push $(CR_IMAGE_NAME):$(GIT_SHORT_COMMIT_HASH)
endif
ifneq ($(IMAGE_TAG)),latest)
	docker push $(CR_IMAGE_NAME):latest
endif

login:
	docker login -u $(CR_USER) -p $(CR_PASSWORD) $(CR_HOST)

tag:
	docker tag $(IMAGE) $(CR_IMAGE)
ifneq ($(GIT_SHORT_COMMIT_HASH),)
	docker tag $(IMAGE_NAME):$(GIT_SHORT_COMMIT_HASH) $(CR_IMAGE_NAME):$(GIT_SHORT_COMMIT_HASH)
endif
ifneq ($(IMAGE_TAG)),latest)
	docker tag $(IMAGE_NAME):latest $(CR_IMAGE_NAME):latest
endif
