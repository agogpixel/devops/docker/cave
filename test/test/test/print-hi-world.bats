load '/opt/bats-support/load.bash'
load '/opt/bats-assert/load.bash'

@test "Prints 'Hi World!'." {
  source ./test/test/print-hi-world.sh
  run print_hi_world
  assert_output 'Hi World!'
}
