load '/opt/bats-support/load.bash'
load '/opt/bats-assert/load.bash'

@test "Prints 'Hello World!'." {
  source ./test/print-hello-world.sh
  run print_hello_world
  assert_output 'Hello World!'
}
