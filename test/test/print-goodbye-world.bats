load '/opt/bats-support/load.bash'
load '/opt/bats-assert/load.bash'

@test "Prints 'Goodbye World!'." {
  source ./test/print-goodbye-world.sh
  run print_goodbye_world
  assert_output 'Goodbye World!'
}
