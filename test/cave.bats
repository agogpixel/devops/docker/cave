load '/opt/bats-support/load.bash'
load '/opt/bats-assert/load.bash'

@test "Prints usage and exits with error when no files provided." {
  source ./cave.sh

  run cave_main

  assert_failure
  assert_output --partial "Usage:"
}

@test "Prints usage and exits with error when invalid option provided." {
  source ./cave.sh

  run cave_main --fail-it

  assert_failure
  assert_output --partial "Usage:"
}

@test "Prints usage and exits with success when -h or --help." {
  source ./cave.sh

  run cave_main -h

  assert_success
  assert_output --partial "Usage:"

  run cave_main --help

  assert_success
  assert_output --partial "Usage:"
}

@test "Basic test." {
  source ./cave.sh

  # Make nested bashcov + bats go.
  export TMPDIR=/tmp

  run cave_main ./test/test/print-hello-world.bats

  assert_success
  assert_output --partial "ok 1 Prints 'Hello World!'."
}

@test "Basic test with '--command-name test-bashcov-command-name'." {
  source ./cave.sh

  # Make nested bashcov + bats go.
  export TMPDIR=/tmp

  local command_name=test-bashcov-command-name

  run cave_main --command-name $command_name ./test/test/print-hello-world.bats

  assert_success
  assert_output --partial "ok 1 Prints 'Hello World!'."

  run cat ./coverage/.resultset.json

  assert_output --partial $command_name
}

@test "Basic test with '--skip-uncovered'." {
  source ./cave.sh

  # Make nested bashcov + bats go.
  export TMPDIR=/tmp

  local test_path=./test/test/print-hello-world.bats

  local resultset_path=./coverage/.resultset.json
  local resultset_bak_path=./coverage/.resultset.json.bak

  local expected_partial_output="ok 1 Prints 'Hello World!'."

  run mv $resultset_path $resultset_bak_path
  run rm -f $resultset_path
  run cave_main --skip-uncovered $test_path

  assert_success
  assert_output --partial "$expected_partial_output"

  run cat $resultset_path

  refute_output --partial 'print-goodbye-world.sh'

  run rm -f $resultset_path
  run mv $resultset_bak_path $resultset_path
  run cave_main --skip-uncovered $test_path

  assert_success
  assert_output --partial "$expected_partial_output"
}

@test "Basic test with directory." {
  source ./cave.sh

  # Make nested bashcov + bats go.
  export TMPDIR=/tmp

  run cave_main ./test/test

  assert_success
  assert_output --partial "ok 1 Prints 'Goodbye World!'."
  assert_output --partial "ok 2 Prints 'Hello World!'."
}

@test "Basic test with directory & '-c' or '--count'." {
  source ./cave.sh

  # Make nested bashcov + bats go.
  export TMPDIR=/tmp

  local test_path=./test/test

  local expected_regexp_output='^(0|[1-9][0-9]*).*'

  run cave_main -c $test_path

  assert_success
  assert_output --regexp $expected_regexp_output

  run cave_main --count $test_path

  assert_success
  assert_output --regexp $expected_regexp_output
}

@test "Basic test with directory & '-f .*Hello[[:space:]]World.*' or '--filter .*Hello[[:space:]]World.*'." {
  source ./cave.sh

  # Make nested bashcov + bats go.
  export TMPDIR=/tmp

  local test_path=./test/test

  local filter=".*Hello[[:space:]]World.*"

  local expected_partial_output="ok 1 Prints 'Hello World!'."
  local excluded_partial_output="Prints 'Goodbye World!'."

  run cave_main -f "$filter" $test_path

  assert_success
  assert_output --partial "$expected_partial_output"
  refute_output --partial "$excluded_partial_output"

  run cave_main --filter "$filter" $test_path

  assert_success
  assert_output --partial "$expected_partial_output"
  refute_output --partial "$excluded_partial_output"
}

@test "Basic test with directory & '-j 2' or '--jobs 2'." {
  source ./cave.sh

  # Make nested bashcov + bats go.
  export TMPDIR=/tmp

  local test_path=./test/test

  local jobs=2

  declare -A expected_partial_output=(
    [goodbye]="ok 1 Prints 'Goodbye World!'."
    [hello]="ok 2 Prints 'Hello World!'."
  )

  run cave_main -j $jobs $test_path

  assert_success
  assert_output --partial "${expected_partial_output[goodbye]}"
  assert_output --partial "${expected_partial_output[hello]}"

  run cave_main --jobs $jobs $test_path

  assert_success
  assert_output --partial "${expected_partial_output[goodbye]}"
  assert_output --partial "${expected_partial_output[hello]}"
}

@test "Basic test with directory & '-r' or '--recursive'." {
  source ./cave.sh

  # Make nested bashcov + bats go.
  export TMPDIR=/tmp

  local test_path=./test/test

  declare -A expected_partial_output=(
    [goodbye]="ok 1 Prints 'Goodbye World!'."
    [hello]="ok 2 Prints 'Hello World!'."
    [hi]="ok 3 Prints 'Hi World!'."
  )

  run cave_main -r $test_path

  assert_success
  assert_output --partial "${expected_partial_output[goodbye]}"
  assert_output --partial "${expected_partial_output[hello]}"
  assert_output --partial "${expected_partial_output[hi]}"

  run cave_main --recursive $test_path

  assert_success
  assert_output --partial "${expected_partial_output[goodbye]}"
  assert_output --partial "${expected_partial_output[hello]}"
  assert_output --partial "${expected_partial_output[hi]}"
}
