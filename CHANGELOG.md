# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Latest]

### Added

- [bats-file](https://github.com/bats-core/bats-file)

## [06d69ca9]

### Changed

- Update [bats-core](https://github.com/bats-core/bats-core) to [1.2.0 release](https://github.com/bats-core/bats-core/releases/tag/v1.2.0).

## [fd2b16ee]

### Added

- [bashcov](https://github.com/infertux/bashcov)
- [bats-core](https://github.com/bats-core/bats-core)
- [bats-support](https://github.com/bats-core/bats-support)
- [bats-assert](https://github.com/bats-core/bats-assert)
- [bats-detik](https://github.com/bats-core/bats-detik)

[Latest]: https://gitlab.com/agogpixel/devops/docker/cave/-/compare/06d69ca9...master
[06d69ca9]: https://gitlab.com/agogpixel/devops/docker/cave/-/tree/06d69ca9
[fd2b16ee]: https://gitlab.com/agogpixel/devops/docker/cave/-/tree/fd2b16ee
