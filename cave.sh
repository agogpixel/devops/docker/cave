#!/bin/bash

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset
#set -o xtrace

################################################################################
# Runtime Settings
################################################################################

# Default to empty.
CAVE_BASHCOV_ARGS=''

# Default to empty.
CAVE_BATS_ARGS=''

################################################################################
# Options
################################################################################

# Print help.
declare -A CAVE_OPT_HELP=([NAME]=help [LONG]=help [SHORT]=h)

# Skip inclusion of uncovered files in report (bashcov).
declare -A CAVE_OPT_SKIP_UNCOVERED=([NAME]=skip-uncovered [LONG]=skip-uncovered)

# Specify command name for coverage report (bashcov).
declare -A CAVE_OPT_COMMAND_NAME=([NAME]=command-name [LONG]=command-name)

# Count the number of test cases without running any tests (bats).
declare -A CAVE_OPT_COUNT=([NAME]=count [LONG]=count [SHORT]=c)

# Filter test cases by names matching the regular expression (bats).
declare -A CAVE_OPT_FILTER=([NAME]=filter [LONG]=filter [SHORT]=f)

# Number of parallel jobs to run (bats).
declare -A CAVE_OPT_JOBS=([NAME]=jobs [LONG]=jobs [SHORT]=j)

# Include tests in subdirectories (bats).
declare -A CAVE_OPT_RECURSIVE=([NAME]=recursive [LONG]=recursive [SHORT]=r)

################################################################################
# Functions
################################################################################

cave_main() {
  cave_parse_args $@
  bashcov $CAVE_BASHCOV_ARGS -- bats $CAVE_BATS_ARGS
}

cave_parse_args() {
  local opts="${CAVE_OPT_HELP[SHORT]}${CAVE_OPT_COUNT[SHORT]}${CAVE_OPT_FILTER[SHORT]}:${CAVE_OPT_JOBS[SHORT]}:${CAVE_OPT_RECURSIVE[SHORT]}"
  local longopts="${CAVE_OPT_HELP[LONG]},${CAVE_OPT_SKIP_UNCOVERED[LONG]},${CAVE_OPT_COMMAND_NAME[LONG]}:,${CAVE_OPT_COUNT[LONG]},${CAVE_OPT_FILTER[LONG]}:,${CAVE_OPT_JOBS[LONG]}:,${CAVE_OPT_RECURSIVE[LONG]}"

  ! PARSED=$(getopt --options=$opts --longoptions=$longopts --name "$0" -- "$@")

  if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has complained about wrong arguments to stdout.
    cave_print_usage
    exit 1
  fi

  # Handle quoting in getopt output.
  eval set -- "$PARSED"

  while true; do
    case "$1" in
      "-${CAVE_OPT_HELP[SHORT]}"|"--${CAVE_OPT_HELP[LONG]}")
        printf '\nbashcov & bats as one command.\n\n'
        printf 'bats         %s\n' "$BATS_VERSION"
        printf 'bats-support %s\n' "$BATS_SUPPORT_VERSION"
        printf 'bats-assert  %s\n' "$BATS_ASSERT_VERSION"
        printf 'bats-detik   %s\n\' "$BATS_DETIK_VERSION"
        printf 'bats-file    %s\n\n' "$BATS_FILE_VERSION"
        cave_print_usage
        printf '\nSee https://gitlab.com/agogpixel/devops/docker/cave for more info.\n'
        exit 0
        ;;
      "--${CAVE_OPT_SKIP_UNCOVERED[LONG]}")
        CAVE_BASHCOV_ARGS="$CAVE_BASHCOV_ARGS --${CAVE_OPT_SKIP_UNCOVERED[LONG]}"
        shift
        ;;
      "--${CAVE_OPT_COMMAND_NAME[LONG]}")
        CAVE_BASHCOV_ARGS="$CAVE_BASHCOV_ARGS --${CAVE_OPT_COMMAND_NAME[LONG]} $2"
        shift 2
        ;;
      "-${CAVE_OPT_COUNT[SHORT]}"|"--${CAVE_OPT_COUNT[LONG]}")
        CAVE_BATS_ARGS="$CAVE_BATS_ARGS -${CAVE_OPT_COUNT[SHORT]}"
        shift
        ;;
      "-${CAVE_OPT_FILTER[SHORT]}"|"--${CAVE_OPT_FILTER[LONG]}")
        CAVE_BATS_ARGS="$CAVE_BATS_ARGS -${CAVE_OPT_FILTER[SHORT]} '$2'"
        shift 2
        ;;
      "-${CAVE_OPT_JOBS[SHORT]}"|"--${CAVE_OPT_JOBS[LONG]}")
        CAVE_BATS_ARGS="$CAVE_BATS_ARGS -${CAVE_OPT_JOBS[SHORT]} ${2}"
        shift 2
        ;;
      "-${CAVE_OPT_RECURSIVE[SHORT]}"|"--${CAVE_OPT_RECURSIVE[LONG]}")
        CAVE_BATS_ARGS="$CAVE_BATS_ARGS -${CAVE_OPT_RECURSIVE[SHORT]}"
        shift
        ;;
      --)
        shift
        break
        ;;
    esac
  done

  if [ "$#" -eq 0 ]; then
    # Must specify at least one test.
    cave_print_usage
    exit 2
  fi

  CAVE_BATS_ARGS="$CAVE_BATS_ARGS $@"
}

cave_print_usage() {
  cat <<EOF
Usage:
  $0 [--${CAVE_OPT_SKIP_UNCOVERED[LONG]}] [--${CAVE_OPT_COMMAND_NAME[LONG]} <name>] [-${CAVE_OPT_COUNT[SHORT]}${CAVE_OPT_RECURSIVE[SHORT]}] [-${CAVE_OPT_FILTER[SHORT]} <regex>] [-${CAVE_OPT_JOBS[SHORT]} <jobs>] <test>...

  bashcov
    Coverage report generated at ./coverage

    --${CAVE_OPT_SKIP_UNCOVERED[LONG]}
      Skip inclusion of uncovered files in report.

    --${CAVE_OPT_COMMAND_NAME[LONG]} <name>
      Specify command name for coverage report.

  bats
    <test> is the path to a Bats test file, or the path to a directory
    containing Bats test files (ending with ".bats").

    -${CAVE_OPT_COUNT[SHORT]}, --${CAVE_OPT_COUNT[LONG]}
      Count the number of test cases without running any tests.

    -${CAVE_OPT_FILTER[SHORT]}, --${CAVE_OPT_FILTER[LONG]}
      Filter test cases by names matching the regular expression.

    -${CAVE_OPT_JOBS[SHORT]}, --${CAVE_OPT_JOBS[LONG]}
      Number of parallel jobs to run.

    -${CAVE_OPT_RECURSIVE[SHORT]}, --${CAVE_OPT_RECURSIVE[LONG]}
      Include tests in subdirectories.
EOF
}

################################################################################
# Program Entry
################################################################################

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  cave_main $@
fi
